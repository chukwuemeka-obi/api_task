const express = require('express')
const router = express.Router()

const paymentController = require('../controllers/paymentController')

router.get('/getTheTotalAmount', paymentController.getTheTotalAmount)
router.get('/getPurchases', paymentController.getPurchases)

module.exports = router
