require('dotenv').config()

module.exports = {
    HOST: process.env.SQL_HOST,
    USER: process.env.SQL_USERNAME,
    PASSWORD: process.env.SQL_PASSWORD,
    DB: process.env.SQL_DATABASE,
    dialect: 'mysql'
}