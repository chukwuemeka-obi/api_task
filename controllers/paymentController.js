const db = require('../models')
require('dotenv').config()

const Payment = db.payments

const defaultAdminAccount = process.env.DEFAULT_ADMIN_ACCOUNT
const getTheTotalAmount = async (req, res) => {
    try {
        let totalAmount = await Payment.findAll({ attributes: [[db.sequelize.fn('sum', db.sequelize.col('amount')), 'total_amount']], where: { isFunding: {[db.Op.eq]: false}, account: {[db.Op.not]: defaultAdminAccount } } })
        res.status(200).send(totalAmount)
    } catch (error) {
        throw new Error(error)
    }
}

const suspenseAccount = process.env.SUSPENSE_ACCOUNT
const getPurchases = async (req, res) => {
    try {
        let purchases = await Payment.findAll({ attributes: ['paymentReference', 'amount', 'account', 'purchase'], where: { isFunding: false, account: {[db.Op.not]: suspenseAccount}, paymentReference: {[db.Op.like]: '%+'}}, group: 'purchase' })
        res.status(200).send(purchases)
    } catch (error) {
        throw new Error(error)
    }
}

module.exports = {
    getTheTotalAmount,
    getPurchases
}