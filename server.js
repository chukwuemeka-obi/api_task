// external module imports
const express = require('express')
const cors = require('cors')
require('dotenv').config()
const paymentRoutes = require('./routes/paymentRoutes')

const app = express()

// app-wide middleware calls
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// routes
app.use('/api/payments', paymentRoutes)

// port
const PORT = process.env.PORT || 3000

// server listening
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT} at http://localhost:${PORT}`)
})
