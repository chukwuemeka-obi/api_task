module.exports = (sequelize, DataTypes) => {
    const Payment = sequelize.define('payments', {
        paymentReference: {
            type: DataTypes.STRING
        },
        amount: {
            type: DataTypes.DOUBLE
        },
        account: {
            type: DataTypes.STRING
        },
        purchase: {
            type: DataTypes.INTEGER
        },
        isFunding: {
            type: DataTypes.BOOLEAN
        }
    })

    return Payment
}
