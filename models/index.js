const dbConfig = require('../config/db')

const {Sequelize, DataTypes, Op} = require('sequelize')
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect
})

async function authenticateDB() {
    try {
        await sequelize.authenticate();
        console.log('Connection to the DB has been established successfully.');
      } catch (error) {
        console.error('Unable to connect to the database:', error);
      }
}

authenticateDB()

const db = {}
db.Sequelize = Sequelize
db.sequelize = sequelize
db.Op = Op

db.payments = require('./paymentModel')(sequelize, DataTypes)

async function syncAllModels() {
    await db.sequelize.sync({ force: true })
    console.log("All models were synchronized successfully.")
}

// syncAllModels()

module.exports = db
